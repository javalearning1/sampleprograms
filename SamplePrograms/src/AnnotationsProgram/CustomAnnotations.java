package AnnotationsProgram;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface Logitech{         //No values inside is marker annotation
     String color();     //one value means single value annotation
     int version();      //Multivalue annotation
}

@Logitech(color = "Black",version = 2)
class Mouse{
    String texture;
    int size;
    Mouse(String texture, int size){
        this.texture = texture;
        this.size=size;
    }
}

public class CustomAnnotations {
    public static void main(String[] args) {
        Mouse mo = new Mouse("Matte", 3 );
        Class m = mo.getClass();
        Annotation an=m.getAnnotation(Logitech.class);
        Logitech lo = (Logitech)an;
        System.out.println(lo.version());

    }
}
