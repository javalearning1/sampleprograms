package AnnotationsProgram;

import java.util.ArrayList;
import java.util.List;

class A{
    public void read(){
        System.out.println("A");
    }
}

class B extends A{
    @Deprecated // Don't use the method
    public void dep(){

    }
    @Override
    @SuppressWarnings("unchecked")
    public void read(){
        System.out.println("B");
        List intList = new ArrayList();
           }
}
@FunctionalInterface //Only one method
interface Z{
    public void print();

}

public class AnnotationsDemo {
    public static void main(String[] args) {
        B obj = new B();
        obj.read();

    }
}
