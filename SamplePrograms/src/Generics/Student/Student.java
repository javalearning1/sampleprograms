package Generics.Student;

class Test<T>{
    T obj;
    Test(T obj){
        this.obj=obj;
    }
    public T getobj(){
        return this.obj;
    }
}

public class Student {
    public static void main(String[] args) {
        Test<String> str = new Test<String>("Nithi");
        System.out.println(str.getobj());

        Test<Integer> tes = new Test<Integer>(100);
        System.out.println(tes.getobj());

    }
}
