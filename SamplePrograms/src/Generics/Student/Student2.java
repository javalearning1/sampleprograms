package Generics.Student;

class Test4u <T,U>{
    T obj1;
    U obj2;

    public Test4u(T obj1, U obj2) {
        this.obj1=obj1;
        this.obj2=obj2;
    }

    public void print(){
        System.out.println(obj1);
        System.out.println(obj2);

    }
}

public class Student2 {
    public static void main(String[] args) {
        Test4u<String,Integer> t = new Test4u<String, Integer>("Nithi", 1);
        t.print();

    }
}
