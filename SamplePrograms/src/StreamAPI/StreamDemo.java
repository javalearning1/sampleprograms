package StreamAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamDemo {
    public static void main(String[] args) {
        ArrayList<Integer> List1 = new ArrayList<Integer>();
        List1.add(50);
        List1.add(48);
        List1.add(27);
        List1.add(10);
        List1.add(7);
        List1.add(6);
        List1.add(3);
        System.out.println(List1);
        List<Integer> list2 = List1.stream().filter(i->i%2==0).collect(Collectors.toList());
        System.out.println(list2);


    }
}
