package FilehandlingProgram;

import java.io.FileWriter;
import java.io.IOException;

public class WriteFile {
    public static void main(String[] args) {
        try {
            FileWriter writer =new FileWriter("D:\\Java\\file.txt");
            writer.write("Java is an object-oriented programming language that produces software for multiple platforms. ");
            writer.close();
            System.out.println("Successfully wrote to file");
        } catch (IOException e){
            System.out.println("An error occurred");
            e.printStackTrace();
        }
    }
}
