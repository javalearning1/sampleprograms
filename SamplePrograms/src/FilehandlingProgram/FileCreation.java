package FilehandlingProgram;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileCreation {
    public static void main(String[] args) {
        try {
            File file = new File("D:\\Java\\file.txt");
            if(file.createNewFile()){
                System.out.println("File is created "+file.getName());
            }else{
                System.out.println("File already exists");
            }
        }catch (IOException e){
            System.out.println("An Error occurred.");
            e.printStackTrace();
        }
        File file=new File("D:\\Java\\file.txt");
        if(file.exists()){
            System.out.println("File name is "+file.getName());
            System.out.println("Path is"+file.getPath());
            System.out.println("Writeable:"+file.canWrite());
            System.out.println("Readable"+file.canRead());
            System.out.println("File size is "+file.length());
        }else{
            System.out.println("File does not exist");
        }


    }
}
