package LambdaExpression;

interface inter{
    void print();
}
public class LambdaProg {
    public static void main(String[] args) {
        inter obj;

       /* obj = new inter() {

            public void print() {
                System.out.println("Print A");
            }
        };*/

        obj = () -> System.out.println("Print n");

        obj.print();

    }
}
