import java.util.*;

public class HashmapEx {
    public static void main(String[] args) {
        Map<String,String> li = new HashMap<>();
        li.put("Nithi", "78387142");
        li.put("Vin", "8345565");
        li.put("Kani", "89895602");
        li.put("Kanya", "89895602"); // Can store duplicate values but not keys and null keys accepted
                                     //HashTable and Treemap does not accept null key values
        li.forEach((k,v) -> System.out.println("Name = "+k+", " +"Mobile Numer = "+ v));

        System.out.println("==========");
        Map<String,String> lit = new TreeMap<>();
        lit.put("Nithi", "78387142");
        lit.put("Vin", "8345565");
        lit.put("Kani", "89895602");
        lit.put("Abi", "89895602");
        lit.forEach((k,v) -> System.out.println("Name = "+k+", " +"Mobile Numer = "+ v));


    }
}
