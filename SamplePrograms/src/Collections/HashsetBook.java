import java.util.HashSet;

class Book{
    String bookname, author;
    int id;
    int quantity;
    public Book(String bookname, String author, int id,int quantity){
        this.bookname = bookname;
        this.author = author;
        this.id = id;
        this.quantity = quantity;
    }

}
public class HashsetBook {
    public static void main(String[] args) {
        HashSet<Book> set = new HashSet<Book>();
        Book b1 = new Book("The Secret", "Mathew", 5020, 100);
        Book b2 = new Book("The Secret Mantra", "Mathew Johness", 5010, 100);
        Book b3 = new Book("The Success Story", "Mathew", 5820, 800);
        set.add(b1);
        set.add(b2);
        set.add(b3);
        for (Book b:set){
            System.out.println(b.id+" "+b.bookname+" "+b.author+" "+b.quantity);
        }
    }
}
