import com.sun.source.tree.Tree;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

public class HashsetEx {
    public static void main(String[] args) {
        HashSet<String> ash = new HashSet<String>();
        ash.add("John");
        ash.add("Nancy");
        ash.add("Clara");
        ash.add("Vandy");
        ash.add("Ravi");

       TreeSet<String> tr = new TreeSet<String>();
        tr.add("Johnny");
        tr.add("Nancy");
        tr.add("Clara");
        tr.add("Vandy");
        tr.add("Ravi");

        Iterator<String> itr= ash.iterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }
        System.out.println("================");

        ArrayList<String> Arl=new ArrayList<String>(tr);
        Arl.add("Vijay");

        Iterator<String> ite= Arl.iterator();
        while (ite.hasNext()){
            System.out.println(ite.next());
        }

    }
}
