import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashsetEx {
    public static void main(String[] args) {
        LinkedHashSet<String>  set = new LinkedHashSet<String>();
        set.add("Pineapple");
        set.add("Banana");
        set.add("Apple");
        set.add("Apple");
        Iterator<String > i = set.iterator();
        while (i.hasNext()){
            System.out.println(i.next());
        }
    }
}
