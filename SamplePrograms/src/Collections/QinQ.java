package Collections;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class QinQ {
    public static void main(String[] args) {
        PriorityQueue<String> q = new PriorityQueue<String>();
        q.add("Ana");
        q.add("Beth");
        q.add("Cinder");
        q.add("Stella");

        System.out.println("Head: "+ q.element());
        System.out.println("Head: "+q.peek());
        System.out.println(q);

        //q.poll();
        //System.out.println(q.peek());
        //System.out.println(q);

    }
}
