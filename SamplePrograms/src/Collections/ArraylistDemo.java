package Collections;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class ArraylistDemo {
    public static void main(String[] args) {
        ArrayList<String> list1 = new ArrayList<String>();
        ArrayList list2 = new ArrayList();

        list1.add("John");
        list1.add("Jenni");
        list1.add("Jeffi");
        list1.add("Jessi");
        list1.add("Jaffi");

        list2.add("Jan");
        list2.add(34);
        list2.add(6.7);

        System.out.println("list1 is " + list1);
        System.out.println("list2 is " + list2);

        String name = list1.get(2);
        System.out.println(name);
        Object  o = list2.get(2);
        System.out.println(o);

        list1.set(2, "Jana");
        System.out.println("Now list1 is "+ list1);
        list1.remove(4);
        System.out.println("Now list1 is "+ list1);

        if(list1.contains("John")){
            System.out.println("List has John");
        }

        for (String s: list1 ){
            System.out.println(s);
        }
        System.out.println("==============");

        Iterator<String> itr = list1.iterator();
        System.out.println(itr.next());
        System.out.println(itr.next());

        LinkedList <String> fruits = new LinkedList<String>();

        fruits.add("Strawberry");
        fruits.add("Blueberry");
        fruits.add("Raspberry");
        fruits.add("Cranberry");
        System.out.println(fruits);

        fruits.remove("Cranberry");
        System.out.println("Now fruits list is "+fruits);


    }


}
