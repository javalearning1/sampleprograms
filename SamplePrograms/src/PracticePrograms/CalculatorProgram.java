package PracticePrograms;

import java.util.Scanner;

public class CalculatorProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the first number");
        int num1 = sc.nextInt();
        System.out.println("Enter the second number");
        int num2 = sc.nextInt();
        System.out.println("Enter the operation");
        char operator = sc.next().charAt(0);
        if (operator == '+' || operator == '_' || operator == '*' || operator == '/') {
            int res = calculate(num1, num2, operator);
            System.out.println("number1" + operator + "number2 is " + res);
        } else
            System.out.println("Enter valid operator");
        sc.close();

    }

     static int calculate(int a, int b, char operator)
    {
        int res = 0;
        switch (operator) {
            case '+':
                res = a+b;
                break;
            case '-':
                res = a-b;
                break;
            case '*':
                res = a*b;
                break;
            case '/':
                res = a/b;
                break;
        }
        return res;
    }
}
