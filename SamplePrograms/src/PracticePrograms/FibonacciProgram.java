package PracticePrograms;

import java.util.Scanner;

public class FibonacciProgram {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("how many fibonacci series");
        int num = scan.nextInt();
        fibonacci(num);
        scan.close();
        }
        static void fibonacci(int n){
        int x=0, y=1, z=0, count=1;
        while(count<=n){
            System.out.println(z+"  ");
            z=x+y;
            x=y;
            y=z;
            count++;
        }

        }
}
