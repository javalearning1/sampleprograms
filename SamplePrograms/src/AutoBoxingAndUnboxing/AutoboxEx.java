package AutoBoxingAndUnboxing;

public class AutoboxEx {
    public static void main(String[] args) {

        int i=10; //Primitive Datatype
        // Integer ii =new Integer(i); //Boxing

        //int j=ii.intValue(); //Unboxing

        Integer a=i; //Autoboxing
        int k=a; //AutoUnboxing

        String s="120";  //Converting String to int
        int n=Integer.parseInt(s);
        System.out.println(k+n);





    }
}
