package OopsConcepts;

public class Inheritance {
        public static void main(String[] args) {
            Mouse1 m = new Mouse1();
            m.texture();
            m.leftClick();
            m.rightClick();
            m.scrollUp();
        }
}
class Mouse {

       public static void leftClick() {
        System.out.println("Click!");
    }
    public static void rightClick() {
        System.out.println("Click!");
    }
    public static void scrollUp() {

        System.out.println("scroll up!");
    }
    public static void scrollDown() {
        System.out.println("Scroll down!");
    }
}
class Mouse1 extends Mouse {
    String texture = "Matte";
    public static void texture(){
        System.out.println("Texture is matte");
    }

}
class Mouse2 extends Mouse {
    public static void connect() {
        System.out.println("connected");
    }
}
class Mouse3 extends Mouse {
    boolean ambi = true;

}