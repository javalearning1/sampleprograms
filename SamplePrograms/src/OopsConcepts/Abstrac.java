package OopsConcepts;

abstract public class Abstrac {
    abstract void sayHello();
    void sayHi() {
        System.out.println("Hi");
    }
}
class Abs extends Abstrac {
    void sayHello() {
        System.out.println("Hello");

    }


    public static void main(String[] args) {
        Abs a = new Abs();
        a.sayHi();
        a.sayHello();
    }

}