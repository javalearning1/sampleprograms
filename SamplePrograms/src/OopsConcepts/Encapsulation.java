package OopsConcepts;

public class Encapsulation {
            public static void main(String[] args) {
            Student s = new Student();
            s.setName("Nithisha");
            System.out.println(s.getName());
            s.setAge(16);
            System.out.println(s.getAge());

        }
}

    class Student {
    private String name;
    private int age;

    public void setName(String newName){

        name = newName;
    }
    public String getName(){
        return name;
    }
    public void setAge(int newAge){
        age = newAge;
    }
    public int getAge(){
        return age;
    }


}
