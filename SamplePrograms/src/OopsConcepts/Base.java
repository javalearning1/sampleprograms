package OopsConcepts;

class Basic {
    static int i;
    int j = 3;
    private String s="Something";

    void meth() {
        System.out.println(j);
    }

    static void metho(int i) {
        System.out.println(i);
    }

    void str(){
        System.out.println(s);
    }
    final void hello(){
        int a=5;
        int b=6;
        System.out.println("a is "+a+" and b is "+b);
    }
    void hello(int a, int b){
        int c=a+b;
    }

}
class Base{

    public static void main(String[] args) {
        Basic b = new Basic();
        b.meth();
        Basic.metho(8);
        b.str();
        //System.out.println(b.s);
        b.hello();
    }
}
