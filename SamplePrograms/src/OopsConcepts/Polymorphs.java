package OopsConcepts;

public class Polymorphs{

    public static void main(String[] args) {
        Sample samp= new Sample();
        Sample1 s = new Sample1();
        s.test();
        samp.test(4);
        samp.test(3,7);

    }
}
class Sample {
    void test() {
        System.out.println("Nothing");
    }

    void test(int a) {
        System.out.println(a);
    }

    void test(int a, int b) {
        System.out.println(a+ b);
    }
}
class Sample1 extends Sample {
    void test() {

    }
}