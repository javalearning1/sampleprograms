package Exception;

public class SampleException {
    public static void main(String[] args) {
     try{
         int a=30, b=0;
         int c=a/b;
         System.out.println("result is "+c);
     }
     catch (ArithmeticException e){
         System.out.println("Can't divide number by zero");
     }
     try{
         int num= Integer.parseInt("its is a string");
         System.out.println(num);
     }
     catch (NumberFormatException e){
         System.out.println("Number format exception");
     }
     try{
         int a[] = new int[5];
         a[7]= 15;
     }
     catch (ArrayIndexOutOfBoundsException e){
         System.out.println("Out of index");
     }
    }
}
