package Exception;

public class ExceptionHandlingMethodThrow {
    static void demo(){
        try{
            throw new Exception("demo");
        }
        catch(Exception e){
            System.out.println("Just a demo");

        }
    }

    public static void main(String[] args) {
        demo();
    }
}
