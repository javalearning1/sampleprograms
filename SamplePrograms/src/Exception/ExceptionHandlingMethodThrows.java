package Exception;

public class ExceptionHandlingMethodThrows {
    static void demo1() throws ArithmeticException {
        System.out.println("Just printing");
        throw new ArithmeticException("demo");
    }

    public static void main(String[] args) {
        try {
            demo1();
        }
        /*finally {
            System.out.println("Finally is always executed");
        }*/
        catch (ArithmeticException e){
            System.out.println("Exception"+ e);
        }
    }

}
