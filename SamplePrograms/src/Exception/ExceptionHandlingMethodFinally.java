package Exception;

public class ExceptionHandlingMethodFinally {
    public static void main(String[] args) {
        try {
            int num = Integer.parseInt("its is a string");
            System.out.println(num);
        }
        finally {
            System.out.println("Finally is always executed");
        }
    }
}
