package Exception;

public class ExceptionHandlingMethodTry {

    public static void main(String[] args) {
        try {
            try {
                int a = 30, b = 0;
                int c = a / b;
                System.out.println("result is " + c);
            } catch (ArithmeticException e) {
                System.out.println("Can't divide number by zero");
            }
            try {
                int num = Integer.parseInt("its is a string");
                System.out.println(num);
            } catch (NumberFormatException e) {
                System.out.println("Number format exception");
            }
            System.out.println("Printing the statement in try block");
        } catch (Exception e) {
            System.out.println("No exception");
        }

    }
}
