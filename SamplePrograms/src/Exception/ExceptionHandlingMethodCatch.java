package Exception;

public class ExceptionHandlingMethodCatch {
    public static void main(String[] args) {
        try {
            int num = Integer.parseInt("its is a string");
            System.out.println(num);
        }
        catch (NumberFormatException e) {
            System.out.println("Number format exception");
        }
        catch (Exception e){
            System.out.println("no exception");
        }
        System.out.println("Just printing");
    }
}
