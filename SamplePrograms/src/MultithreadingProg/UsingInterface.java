package MultithreadingProg;
class SayHi implements Runnable {
    public void run() {
        for (int i = 0; i < 5; i++){
            System.out.println("Say Hi");
            try{Thread.sleep(1000);} catch(Exception e){}
        }
    }
}
class SayHello implements Runnable{
    public void run() {
        for (int i = 0; i < 5; i++){
            System.out.println("Say Hello");
            try{Thread.sleep(1000);} catch(Exception e){}
        }
    }

}

public class UsingInterface {
    public static void main(String[] args) {
        Runnable obj1=new SayHi();
        Runnable obj2=new SayHello();
        Thread t1 = new Thread(obj1);
        Thread t2 = new Thread(obj2);
        t1.start();
        t2.start();

    }
}
