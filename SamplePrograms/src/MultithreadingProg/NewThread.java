package MultithreadingProg;

class Thread_A extends Thread {
    public void run() {
        for (int i = 0; i < 5; i++){
            System.out.println("Print A");
            try{Thread.sleep(1000);} catch(Exception e){}
        }
    }
}
class Thread_B extends Thread{
    public void run() {
        for (int i = 0; i < 5; i++){
            System.out.println("Print B");
            try{Thread.sleep(1000);} catch(Exception e){}
        }
    }

}

public class NewThread {
    public static void main(String[] args) {
        Thread_A obj1=new Thread_A();
        Thread_B obj2=new Thread_B();
        obj1.start();
        obj2.start();


    }
}
