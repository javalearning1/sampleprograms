package MultithreadingProg;

public class UsingLambda {
    public static void main(String[] args) throws Exception {

        Thread t1 = new Thread(() ->
        {
        for (int i = 0; i < 2; i++)
            {
                System.out.println("Say Hi");
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {}
            }
        },"Hi Thread");        ;
        Thread t2 = new Thread(() ->
        {
        for (int i = 0; i < 2; i++)
           {
               System.out.println("Say Hello");
               try {
                Thread.sleep(1000);
            } catch (Exception e) {}
        }
        },"HelloThread");

        t1.setPriority(1);
        System.out.println(t1.getPriority());

        t1.start();
        t2.start();

        t1.join();
        t2.join();
        System.out.println(t1.getName());

        System.out.println("Bye");
        System.out.println(t2.isAlive());

    }
}