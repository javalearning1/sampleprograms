package MultithreadingProg;

class IncrementClass{
    int count;
    public synchronized void increment(){
        count++;
    }
}

public class SynchronizedThread {
    public static void main(String[] args) throws Exception {
        IncrementClass incr = new IncrementClass();
        Thread t1 = new Thread(new Runnable()
                {
                    public void run() {
                        for (int i = 0; i < 1000; i++) {
                            incr.increment();
                        }
                    }
                });
        Thread t2 = new Thread(new Runnable()
        {
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    incr.increment();
                }
            }
                });

        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("Count is "+ incr.count);

    }
}
