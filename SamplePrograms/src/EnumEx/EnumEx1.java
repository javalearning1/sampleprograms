package EnumEx;

public class EnumEx1 {
    enum days {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
    }

    public static void main(String[] args) {
        for(days d:days.values()){
            weekend(d);
        }

    }
    public static void weekend(days d){
        if((d == days.Sunday)||(d==days.Saturday) ){
            System.out.println(d+" is Holiday");
        }else {
            System.out.println(d+" is Working");
        }
    }

}
