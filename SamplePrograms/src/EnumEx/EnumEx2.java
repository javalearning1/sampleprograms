package EnumEx;
enum trafficSignal{
    Red("STOP"), Yellow("Yield"),Green("Go");
    private String action;

    public String getAction(){
        return this.action;
    }
    private trafficSignal(String action){
        this.action=action;
    }

}

public class EnumEx2 {
    public static void main(String[] args) {
        trafficSignal[] sig= trafficSignal.values();
        for(trafficSignal s:sig){
            System.out.println("name:"+s.name()+" and Action:"+s.getAction());
        }

    }
}
