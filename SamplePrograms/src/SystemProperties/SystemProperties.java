package SystemProperties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class SystemProperties {
    public static void main(String[] args) throws Exception {
        /*String javaVersion = System.getProperty("java.version");
        String javaHome= System.getProperty("java.home");
        String osName = System.getProperty("os.name");
        System.out.println("Java version is"+ javaVersion);
        System.out.println("Java version is"+ javaHome);
        System.out.println("Java version is"+ osName); */
        //To set properties to a file

        Properties p =new Properties();
        OutputStream fo = new FileOutputStream("data.properties");
        p.setProperty("url", "localhost:4250/db");
        p.setProperty("username", "Nithisha");
        p.setProperty("Password", "password");
        p.store(fo,null);

        /* To fetch from a property file
        Properties p=new Properties();
        InputStream is = new FileInputStream("data.properties");
        p.load(is); */

        System.out.println(p.getProperty("username"));


    }
}
