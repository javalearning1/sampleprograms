public class Sample {
    public static void main(String[] args) {
        print();

        int sum = add(4, 5);
        System.out.println(sum);

        String str = caps( "WISH all LOADS OF HAPPINESS");
        System.out.println(str);

        int[] sampArr = arrayEx(3,5,9);
        System.out.println(sampArr[0]);
        System.out.println(sampArr[1]);
        System.out.println(sampArr[2]);

    }
    public static void print(){
        System.out.println("Just Print");
    }

    public static int add(int a, int b) {
        return a+b;
    }
    public static String caps(String str){
        return str.toLowerCase();
    }
    public static int[] arrayEx(int a,int b, int c){
        int[] arra = new int[3];
        arra[0] = a;
        arra[1] = b;
        arra[2] = c;
        return arra;
    }
}
